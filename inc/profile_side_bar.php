   <!-- content -->
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-2 bg-dark" style="min-height: 1000px">
          <div class="mt-4">
            <?php
              include "inc/db_conn.php";

              $view_profile = "SELECT * FROM team WHERE fname = '$_GET[name]'";
              $run_view_profile = mysqli_query($conn, $view_profile);

              while($res_view_profile = mysqli_fetch_array($run_view_profile)){

            ?>
            <a href="profile.php?name=<?php echo $_GET['name'] ?>"><img class="rounded-circle d-block mx-auto" src="img/team/<?php echo $res_view_profile['image']; ?>" width="100px" height="100px"></a>
          <?php } ?>
          </div>

          <h5 class="text-center text-light my-3"><?php echo $_GET['name'] ?></h5>
          <div class="list-group mt-4 mb-3 text-center" style="font-size: 13px">
              <div class="accordion" style="cursor: pointer">
                <a href="profile.php?name=<?php echo $_GET['name'] ?>" class="card-title text-light" style="text-decoration: none">
                  <div class="card-header collapsed" data-toggle="collapse"  data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                    <i class="fas fa-user-alt" style="color: #1BBCED"></i> &nbsp; Personal Details
                  </div>
                  </a>
              </div>
              <div class="accordion" style="cursor: pointer">
                <a href="view_attendance.php?name=<?php echo $_GET['name'] ?>" class="card-title text-light" style="text-decoration: none">
                  <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                    <i class="fas fa-info" style="color: #1BBCED"></i> &nbsp; View Attendance
                  </div>
                  </a>
              </div>
          </div>
        </div>