<body onload="startTime()">

    <!-- navbar -->
    <nav class="navbar sticky-top navbar-expand-md navbar-light bg-dark">
      <a class="navbar-brand" href="home.php" style="color: #fff"><span style="color: #1BBCED">TFN</span> Attendance System</a>
      <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto" style="font-size: 12px; color: #ccc">
          <li class="nav-item active p-2">
            <img src="img/srilanka.png" class="rounded-circle d-inline mr-2" width="20px" height="20px">
            <span id="clockbox1"></span>
          <li class="nav-item active p-2">
            <img src="img/usa.png" class="rounded-circle d-inline mr-2" width="20px" height="20px">
            <span id="clockbox"></span>
          </li>
        </ul>
      </div>
    </nav>

    <nav class="navbar container-fluid navbar-expand-md navbar-light bg-dark">
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto" style="font-size: 15px">
          <?php
            include "db_conn.php";

            $get_team = "SELECT * FROM team ORDER BY fname";
            $run_get_team = mysqli_query($conn,$get_team);

            while($res_get_team = mysqli_fetch_array($run_get_team)){

          ?>
          <li class="nav-item active p-2">
            <a class="nav-link d-inline text-light" href="profile.php?name=<?php echo $res_get_team['fname'] ?>"><img src="img/team/<?php echo $res_get_team['image'] ?>" class="rounded-circle d-inline mr-2" width="35px" height="35px" style="border: 1.5px #fff solid">
            <?php echo $res_get_team['fname'] ?></a>
          </li>
        <?php } ?>
        </ul>

        <div class="btn-group dropleft float-right">
          <button type="button" class="btn btn-sm dropdown-toggle text-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="user">
           <i class="fas fa-user-alt"></i>
          </button>
          <div class="dropdown-menu" style="font-size: 14px">
            <a class="dropdown-item" data-toggle="modal" data-target="#Modal1" title="Register" style="cursor: pointer"><i class="fas fa-user-edit" style="font-size: 12px"></i> &nbsp; Member Registration</a>
            <a class="dropdown-item" data-toggle="modal" data-target="#Modal2" title="Sign in" style="cursor: pointer"><i class="fas fa-key" style="font-size: 12px"></i> &nbsp; Sign in</a>
          </div>
        </div>

        <a class="nav-link text-light" href="login_logout_query/logout_query.php" style="float: right" title="logout"><i class="fas fa-power-off"></i></a>

      </div>
    </nav>
    <!-- navbar -->

    <!-- popper for Inqiry form -->
    <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true ">
      <div class="modal-dialog col-md-3" role="document ">
          <div class="modal-content">
              <div class="modal-header bg-dark text-light" style="border-radius: 0">
                  <h5 class="modal-title" id="exampleModalLabel">Team Member Registraion</h5>
                  <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body p-4">
                  <form action="registration_query/register_member_query.php" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">First Name</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="far fa-user"></i></span>
                          </div>
                          <input type="text" name="fname" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" placeholder="First Name" required>
                          <div class="invalid-feedback">
                            Please insert first name.
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Full Name</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-user-alt"></i></span>
                          </div>
                          <input type="text" name="full_name" class="form-control" id="validationCustomUsername" placeholder="Full Name" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Employee ID</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-id-badge"></i></span>
                          </div>
                          <input type="text" name="emp_id" class="form-control" id="validationCustomUsername" placeholder="Employee No" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Date of Appointment</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-id-badge"></i></span>
                          </div>
                          <input type="text" name="doa" class="form-control" id="validationCustomUsername" placeholder="Eg: 01 Feb 2019" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Designation</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-briefcase"></i></span>
                          </div>
                          <input type="text" name="designation" class="form-control" id="validationCustomUsername" placeholder="Designation" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Date of Birth</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-birthday-cake"></i></span>
                          </div>
                          <input type="text" name="dob" class="form-control" id="validationCustomUsername" placeholder="Eg: 06 Nov 1993" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Contact No</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-mobile-alt"></i></span>
                          </div>
                          <input type="text" name="contact" class="form-control" id="validationCustomUsername" placeholder="Contact No" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Address</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-map-marker"></i></span>
                          </div>
                          <input type="text" name="address" class="form-control" id="validationCustomUsername" placeholder="Address" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Skype ID</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fab fa-skype"></i></span>
                          </div>
                          <input type="text" name="skype" class="form-control" id="validationCustomUsername" placeholder="Skype ID" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Image</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-image"></i></span>
                          </div>
                          <input type="file" name="img" class="form-control" id="validationCustomUsername" placeholder="Skype ID" aria-describedby="inputGroupPrepend">
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Office Mail</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-envelope"></i></span>
                          </div>
                          <input type="email" name="mail" class="form-control" id="validationCustomUsername" placeholder="Office Mail" aria-describedby="inputGroupPrepend" required>
                          <div class="invalid-feedback">
                            Please insert office mail.
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Password</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
                          </div>
                          <input type="password" name="pwd" class="form-control" id="validationCustomUsername" placeholder="Password" aria-describedby="inputGroupPrepend" required>
                          <div class="invalid-feedback">
                            Please insert your password.
                          </div>
                        </div>
                      </div>
                    </div>
                    <button class="btn btn-success w-100 mt-3" type="submit" name="submit">Register Now</button>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- popper for Inqiry form -->

    <!-- popper for Inqiry form -->
    <div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 10%">
      <div class="modal-dialog col-md-3" role="document ">
          <div class="modal-content">
              <div class="modal-header bg-dark text-light" style="border-radius: 0">
                  <h5 class="modal-title" id="exampleModalLabel">Sign In</h5>
                  <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body p-4">
                  <form action="admin/login_logout_query/login_query.php" method="POST" class="needs-validation" novalidate>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Mail</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-envelope"></i></span>
                          </div>
                          <input type="email" name="mail" class="form-control" id="validationCustomUsername" placeholder="example@thefuturenet.com" aria-describedby="inputGroupPrepend" required>
                          <div class="invalid-feedback">
                            Please insert office mail.
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="col-md-12 mb-3">
                        <label for="validationCustomUsername">Password</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
                          </div>
                          <input type="password" name="pwd" class="form-control" id="validationCustomUsername" placeholder="Password" aria-describedby="inputGroupPrepend" required>
                          <div class="invalid-feedback">
                            Please insert your password.
                          </div>
                        </div>
                      </div>
                    </div>
                    <button class="btn btn-success w-100 mt-3" type="submit" name="submit">Sign in Now</button>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- popper for Inqiry form -->