<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:index.php');
}
?>

<?php 
  include "inc/head.php";
  include "inc/navbar.php";
?>
    
    <!-- breadcrumb -->
    <nav aria-label="breadcrumb" style="font-size: 14px">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="home.php" style="color: #000; text-decoration: none; font-weight: 500">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Profile</li>
        </ol>
    </nav>
    <!-- breadcrumb -->

    <!-- content -->
    <?php
      include "inc/profile_side_bar.php";
    ?>

        <div class="col-sm-10 bg-light px-0">
          <div class="px-3">
            <h5 class="text-uppercase mt-4 mb-3">Personal Details</h5>
            <hr>
            <div class="bg-white"> 
              <table class="table border-bottom">
                <?php
                  include "inc/db_conn.php";

                  $view_profile = "SELECT * FROM team WHERE fname = '$_GET[name]'";
                  $run_view_profile = mysqli_query($conn, $view_profile);

                  while($res_view_profile = mysqli_fetch_array($run_view_profile)){

                ?>
                <tr>
                  <td width="200px"><i class="far fa-user"></i> &nbsp; First Name</td>
                  <td><?php echo $res_view_profile['fname'] ?></td>
                </tr><tr>
                  <td><i class="fas fa-user-alt"></i> &nbsp; Full Name</td>
                  <td><?php echo $res_view_profile['full_name'] ?></td>
                </tr>
                <tr>
                  <td><i class="fas fa-id-badge"></i> &nbsp; Employee ID</td>
                  <td><?php echo $res_view_profile['emp_id'] ?></td>
                </tr>
                <tr>
                  <td><i class="fas fa-id-badge"></i> &nbsp; Date of Appointment</td>
                  <td><?php echo $res_view_profile['date_of_appointment'] ?></td>
                </tr>
                <tr>
                  <td><i class="fas fa-briefcase"></i> &nbsp; Designation</td>
                  <td><?php echo $res_view_profile['designation'] ?></td>
                </tr>
                <tr>
                  <td><i class="fas fa-birthday-cake"></i> &nbsp; DOB</td>
                  <td><?php echo $res_view_profile['dob'] ?></td>
                </tr>
                <tr>
                  <td><i class="fas fa-mobile-alt"></i> &nbsp; Contact No</td>
                  <td><?php echo $res_view_profile['contact'] ?></td>
                </tr>
                <tr>
                  <td><i class="fas fa-map-marker"></i> &nbsp; Address</td>
                  <td><?php echo $res_view_profile['address'] ?></td>
                </tr>
                <tr>
                  <td><i class="fab fa-skype"></i> &nbsp; Skype ID</td>
                  <td><?php echo $res_view_profile['skype'] ?></td>
                </tr>
                <tr>
                  <td><i class="fas fa-envelope"></i> &nbsp; Office E-mail</td>
                  <td><?php echo $res_view_profile['mail'] ?></td>
                </tr>
              <?php } ?>
              </table>        
            </div>
          </div>

        </div>  
      </div>    
    </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?>