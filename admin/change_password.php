<?php
session_start();
if(!isset($_SESSION['team_mail'])){
   header('location:home.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/admin_side_bar.php";
?>  
    
    <div class="col-sm-2 col-md-9 col-lg-10 col-xl-10 bg-light px-0">
          <nav aria-label="breadcrumb" style="font-size: 14px">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard.php" style="color: #000; text-decoration: none; font-weight: 500">Profile</a></li>
                <li class="breadcrumb-item active" aria-current="page">Change Password</li>
              </ol>
          </nav>

          <?php
	          if(isset($_GET['success_msg'])){
	        ?>
                <div class="alert alert-success alert-block mx-3">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
                </div>

	        <?php }elseif(isset($_GET['fail_msg'])){ ?>

	            <div class="alert alert-danger alert-block mx-3">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
                </div>
	        <?php } ?>

          <h5 class="text-uppercase ml-3 mt-4 mb-3">Change Password</h5>
          	<div class="container bg-white my-3 pt-3">
	            <form action="profile_query/change_password_query.php" method="POST" class="needs-validation" novalidate>
				  <div class="form-row">
				    <div class="col-md-12 mb-3">
				      <label for="validationCustomUsername">Old Password</label>
				      <div class="input-group">
				        <div class="input-group-prepend">
				          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key text-danger"></i></span>
				        </div>
				        <input type="password" name="pwd_old" class="form-control" id="validationCustomUsername" placeholder="Old Password" aria-describedby="inputGroupPrepend" required>
				        <div class="invalid-feedback">
				          Please insert your old password.
				        </div>
				      </div>
				    </div>
				  </div>
				  <div class="form-row">
				    <div class="col-md-12 mb-3">
				      <label for="validationCustomUsername">New Password</label>
				      <div class="input-group">
				        <div class="input-group-prepend">
				          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
				        </div>
				        <input type="password" name="pwd_new" class="form-control" id="validationCustomUsername" placeholder="New Password" aria-describedby="inputGroupPrepend" required>
				        <div class="invalid-feedback">
				          Please insert your new password.
				        </div>
				      </div>
				    </div>
				  </div>
				  <div class="form-row">
				    <div class="col-md-12 mb-3">
				      <label for="validationCustomUsername">Confirm Password</label>
				      <div class="input-group">
				        <div class="input-group-prepend">
				          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
				        </div>
				        <input type="password" name="pwd_confirm" class="form-control" id="validationCustomUsername" placeholder="Confirm Password" aria-describedby="inputGroupPrepend" required>
				        <div class="invalid-feedback">
				          Please confirm your password.
				        </div>
				      </div>
				    </div>
				  </div>

				  <input type="hidden" name="id" value="<?php echo $_SESSION['id'] ?>">
				  <button class="btn btn-success w-100 mt-3" type="submit" name="submit">Change now</button>
			</form>
          </div>
        </div>  
      </div>    
    </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?> 