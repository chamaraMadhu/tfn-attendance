<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- datatable -->
    <link href="../css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- datatable -->

    <title>TFN dashboard</title>

    <style>
      .accordion{
        background-color: #6B6B6B;
      }

      .accordion:hover{
        background-color: #000;
      }
    </style>
  </head>
  <body>
    
    <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
      <a class="navbar-brand text-white" href="dashboard.php"><span style="color: #1BBCED">TFN</span> TEAM DASHBOARD</a>
      <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto" style="font-size: 13px">
          <li class="nav-item">
            <span class="nav-link text-light"><i class="fas fa-envelope"></i> : chamara@thefuturenet.com</span>
          </li>
          <li class="nav-item">
            <a class="nav-link text-light" href="login_logout_query/logout_query.php"><i class="fas fa-power-off"></i> Logout</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-light" href="dashboard.php"><img src="../img/team/<?php echo $_SESSION['img']; ?>" class="rounded-circle d-inline" width="30px" height="30px" style="margin-top: -7px"></a>
          </li>
        </ul>
      </div>
    </nav>
    <!-- navbar -->