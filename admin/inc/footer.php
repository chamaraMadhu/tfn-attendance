    <!-- footer -->
    <div class="container-fluid bg-dark fixed-bottom">
      <div class="row">
        <div class="col"><p class="text-light text-center pt-3" style="font-size: 13px">All right reserved. Design and develop by Chamara Madhushanka</p></div>
      </div>
    </div> 
    <!-- footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- form validation -->
    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>  
    <!-- form validation -->

    <!-- data table Files -->
    <script src="../js/datatables.min.js"></script>
    <script>
      $('#zero_config').DataTable();
    </script>
    <!-- data table Files -->

    <!-- alert for attendance record deletion -->
    <script>
        $(".del_attendance").click(function(){
            if(confirm("Are you sure to delete this record?")){
                return true;
            }
            return false;
        });
    </script>
    <!-- alert for attendance record deletion -->

    <!-- alert for account deletion -->
    <script>
        $(".del_account").click(function(){
            if(confirm("Are you sure to delete this profile?")){
                return true;
            }
            return false;
        });
    </script>
    <!-- alert for account deletion -->

  </body>
</html>