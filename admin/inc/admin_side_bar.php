<!-- content -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2 col-md-3 col-lg-2 bg-dark" style="min-height: 1000px">
          <div class="mt-4">
            <a href="dashboard.php"><img class="rounded-circle d-block mx-auto" src="../img/team/<?php echo $_SESSION['img']; ?>" width="100px" height="100px"></a>
          </div>

          <h5 class="text-center text-light my-3">Chamara</h5>
          <a href="edit_profile.php" class="btn btn-success btn-sm d-block mx-auto mb-4"><i class="fas fa-user-edit"></i> Update Profile</a>
          <div class="list-group mb-3 text-center">
              <div class="accordion" style="cursor: pointer">
                <a href="add_attendance.php" class="card-title text-light" style="font-size: 14px; text-decoration: none">
                  <div class="card-header collapsed" data-toggle="collapse"  data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                    <i class="fas fa-plus" style="color: #1BBCED"></i> &nbsp; Add Attendance
                  </div>
                  </a>
              </div>
              <div class="accordion" style="cursor: pointer">
                <a href="view_attendance.php" class="card-title text-light" style="font-size: 14px; text-decoration: none">
                  <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                    <i class="fas fa-info" style="color: #1BBCED"></i> &nbsp; View Atendance
                  </div>
                  </a>
              </div>
              <div class="accordion" style="cursor: pointer">
                <a href="change_password.php" class="card-title text-light" style="font-size: 14px; text-decoration: none">
                  <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                    <i class="fas fa-key" style="color: #1BBCED"></i> &nbsp; Change Password
                  </div>
                  </a>
              </div>
              <div class="accordion" style="cursor: pointer">
                <a href="profile_query/delete_profile_query.php?id=<?php echo $_SESSION['id']; ?>" class="card-title text-light del_account" style="font-size: 14px; text-decoration: none">
                  <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                    <i class="fas fa-trash-alt" style="color: #1BBCED"></i> &nbsp; Delete Account
                  </div>
                  </a>
              </div>
          </div>
        </div>