<?php
session_start();
if(!isset($_SESSION['team_mail'])){
   header('location:../home.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/admin_side_bar.php";
?>  

	<div class="col-sm-2 col-md-9 col-lg-10 col-xl-10 bg-light px-0">
          <nav aria-label="breadcrumb" style="font-size: 14px">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard.php" style="color: #000; text-decoration: none; font-weight: 500">Profile</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Attendance</li>
              </ol>
          </nav>

          <?php
	          if(isset($_GET['success_msg'])){
	        ?>
	                    <div class="alert alert-success alert-block">
	                        <button type="button" class="close" data-dismiss="alert">x</button>
	                        <strong><?php echo $_GET['success_msg']; ?> </strong> 
	                    </div>

	        <?php }elseif(isset($_GET['fail_msg'])){ ?>

	            <div class="alert alert-danger alert-block">
	                        <button type="button" class="close" data-dismiss="alert">x</button>
	                        <strong><?php echo $_GET['fail_msg']; ?></strong> 
	                    </div>
	        <?php } ?>

          <h5 class="text-uppercase ml-3 mt-4 mb-3">Edit attendance</h5>
          	<div class="container bg-white my-3 pt-3">
          		<?php
                    include "../inc/db_conn.php";

                    $view_attendance = "SELECT * FROM attendance WHERE id = '$_GET[id]'";
                    $run_view_attendance = mysqli_query($conn, $view_attendance);

                    while($res_view_attendance = mysqli_fetch_array($run_view_attendance)){

                 ?>
	            <form action="attendance_query/edit_attendance_record_query.php" method="GET" class="needs-validation" novalidate>
	            	  <div class="form-row">
					    <div class="col-md-12 mb-3">
					      <label for="validationCustomUsername">Date</label>
					      <div class="input-group">
					        <div class="input-group-prepend">
					          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-calendar-alt"></i></span>
					        </div>
					        <input type="date" name="w_date" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_view_attendance['w_date'] ?>" required>
					        <div class="invalid-feedback">
					          Please insert the date.
					        </div>
					      </div>
					    </div>
					  </div>
					  <div class="form-row">
					    <div class="col-md-6 mb-3">
					      <label for="validationCustomUsername">Punch In</label>
					      <div class="input-group">
					        <div class="input-group-prepend">
					          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-fingerprint"></i></span>
					        </div>
					        <input type="text" name="punch_in" class="form-control" id="validationCustomUsername" placeholder="Punch In" aria-describedby="inputGroupPrepend" value="<?php echo $res_view_attendance['punch_in'] ?>" required>
					        <div class="invalid-feedback">
					          Please insert your punch in time.
					        </div>
					      </div>
					    </div>
					    <div class="col-md-6 mb-3">
					      <label for="validationCustomUsername">Punch out</label>
					      <div class="input-group">
					        <div class="input-group-prepend">
					          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-fingerprint"></i></span>
					        </div>
					        <input type="text" name="punch_out" class="form-control" id="validationCustomUsername" placeholder="Punch Out" aria-describedby="inputGroupPrepend" value="<?php echo $res_view_attendance['punch_out'] ?>" required>
					        <div class="invalid-feedback">
					          Please insert your punch out time.
					        </div>
					      </div>
					    </div>
					  </div>
					  <div class="form-row">
					    <div class="col-md-6 mb-3">
					      <label for="validationCustomUsername">Lunch In</label>
					      <div class="input-group">
					        <div class="input-group-prepend">
					          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-utensils"></i></span>
					        </div>
					        <input type="text" name="lunch_in" class="form-control" id="validationCustomUsername" placeholder="Lunch In" aria-describedby="inputGroupPrepend" value="<?php echo $res_view_attendance['lunch_in'] ?>" required>
					        <div class="invalid-feedback">
					          Please insert your lunch in time.
					        </div>
					      </div>
					    </div>
					    <div class="col-md-6 mb-3">
					      <label for="validationCustomUsername">Lunch Out</label>
					      <div class="input-group">
					        <div class="input-group-prepend">
					          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-utensils"></i></span>
					        </div>
					        <input type="text" name="lunch_out" class="form-control" id="validationCustomUsername" placeholder="Lunch Out" aria-describedby="inputGroupPrepend" value="<?php echo $res_view_attendance['lunch_out'] ?>" required>
					        <div class="invalid-feedback">
					          Please insert your lunch out time.
					        </div>
					      </div>
					    </div>
					  </div>
					  <div class="form-row">
					    <div class="col-md-6 mb-3">
					      <label for="validationCustomUsername">Break In</label>
					      <div class="input-group">
					        <div class="input-group-prepend">
					          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-mug-hot"></i></span>
					        </div>
					        <input type="text" name="break_in" class="form-control" id="validationCustomUsername" placeholder="Break In" aria-describedby="inputGroupPrepend" value="<?php echo $res_view_attendance['break_in'] ?>" required>
					        <div class="invalid-feedback">
					          Please choose a username.
					        </div>
					      </div>
					    </div>
					    <div class="col-md-6 mb-3">
					      <label for="validationCustomUsername">Break Out</label>
					      <div class="input-group">
					        <div class="input-group-prepend">
					          <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-mug-hot"></i></span>
					        </div>
					        <input type="text" name="break_out" class="form-control" id="validationCustomUsername" placeholder="Break Out" aria-describedby="inputGroupPrepend" value="<?php echo $res_view_attendance['break_out'] ?>" required>
					        <div class="invalid-feedback">
					          Please choose a username.
					        </div>
					      </div>
					    </div>
					  </div>
					  <input type="hidden" name="id" value="<?php echo $res_view_attendance['id'] ?>">
					  <input type="hidden" name="name" value="<?php echo $res_view_attendance['name'] ?>">
					  <button class="btn btn-success w-100 mt-3" type="submit" name="submit">Edit now</button>
				</form>
				<?php } ?>
			</div>
          </div>
        </div>  
      </div>    
    </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?>