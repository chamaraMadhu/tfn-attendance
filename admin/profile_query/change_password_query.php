<?php
include "../../inc/db_conn.php";

if(isset($_POST['submit'])){

	$sql = "SELECT * FROM team WHERE id = '$_POST[id]'";
	$run = mysqli_query($conn,$sql);

	while($res = mysqli_fetch_array($run)){
		$old_pwd = $res['password'];
	}

	$pwd_old_entered = base64_encode($_POST['pwd_old']);

	if ($old_pwd == $pwd_old_entered) {

		$pwd_new = base64_encode($_POST['pwd_new']);
		$pwd_new_confirm = base64_encode($_POST['pwd_confirm']);

		if ($pwd_new == $pwd_new_confirm) {

			if ($pwd_new !== $old_pwd) {

				$password_reset = "UPDATE team SET password = '$pwd_new' WHERE id = '$_POST[id]'";
				$run_password_reset = mysqli_query($conn,$password_reset);

				if($run_password_reset > 0){
					$message = "Password has been updated successfully !";
					header("location:../change_password.php?success_msg=".$message);
				
				}else{

					$message = "Password has not been updated successfully !";
					header("location:../change_password.php?fail_msg=".$message);
				}

			}else{

				$message = "Old and new passwords you entered are same !";
				header("location:../change_password.php?fail_msg=".$message);
			}

		}else{

			$message = "Please check the password confirmation !";
			header("location:../change_password.php?fail_msg=".$message);
		}

	}else{

		$message = "Please check the old password you entered. It is not correct !";
		header("location:../change_password.php?fail_msg=".$message);
	}

	
}
?>