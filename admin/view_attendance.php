<?php
session_start();
if(!isset($_SESSION['team_mail'])){
   header('location:../home.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/admin_side_bar.php";
?>  

     <div class="col-sm-10 bg-light px-0">
          <nav aria-label="breadcrumb" style="font-size: 14px">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard.php" style="color: #000; text-decoration: none; font-weight: 500">Profile</a></li>
                <li class="breadcrumb-item active" aria-current="page">View Attendance</li>
              </ol>
          </nav>

          <?php
            if(isset($_GET['success_msg'])){
          ?>
              <div class="alert alert-success alert-block mx-3">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['success_msg']; ?> </strong> 
              </div>

          <?php }elseif(isset($_GET['fail_msg'])){ ?>

              <div class="alert alert-danger alert-block mx-3">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_msg']; ?></strong> 
              </div>
          <?php } ?>

          <h5 class="text-uppercase ml-3 mt-4 mb-3">View attendance</h5>
            <div class="container bg-white my-3 p-3">
              <table id="zero_config" class="table table-hover text-center" style="font-size: 14px">
                <thead>
                    <tr style="background-color: gray; color:#fff">
                        <td>Date</td>
                        <td>Punch In</td>
                        <td>Punch Out</td>
                        <td>Lunch In</td>
                        <td>Lunch Out</td>
                        <td>Break In</td>
                        <td>Break Out</td>
                        <td width="65px">Action</td>
                    </tr>
                </thead>

                <tbody>
                  <?php
                    include "../inc/db_conn.php";

                    $view_attendance = "SELECT * FROM attendance WHERE name = '$_SESSION[fname]'";
                    $run_view_attendance = mysqli_query($conn, $view_attendance);

                    while($res_view_attendance = mysqli_fetch_array($run_view_attendance)){

                  ?>
                    <tr>
                      <td><?php echo $res_view_attendance['w_date'] ?></td>
                      <td><?php echo $res_view_attendance['punch_in'] ?></td>
                      <td><?php echo $res_view_attendance['punch_out'] ?></td>
                      <td><?php echo $res_view_attendance['lunch_in'] ?></td>
                      <td><?php echo $res_view_attendance['lunch_out'] ?></td>
                      <td><?php echo $res_view_attendance['break_in'] ?></td>
                      <td><?php echo $res_view_attendance['break_out'] ?></td>
                      <td>
                        <form class="d-inline" method="GET" action="edit_attendance.php">
                            <input type="hidden" name="id" value="<?php echo $res_view_attendance['id'] ?>"/>
                            <button type="submit" class="btn btn-secondary btn-sm mr-1"><i class="fas fa-pencil-alt"></i></button>
                        </form>
                        <form class="d-inline" method="GET" action="attendance_query/delete_attendance_record_query.php">
                            <input type="hidden" name="id" value="<?php echo $res_view_attendance['id'] ?>"/>
                            <button type="submit" name="submit" class="btn btn-danger btn-sm del_attendance"><i class="fas fa-trash-alt"></i></button>
                        </form>
                      </td>
                    </tr>    
                    <?php } ?>       
                </tfoot>
              </table> 
          </div>
        </div>  
      </div>    
    </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?> 