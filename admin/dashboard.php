<?php
session_start();
if(!isset($_SESSION['team_mail'])){
   header('location:../home.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/admin_side_bar.php";
?>        

        <div class="col-sm-10 bg-light px-0">
          <nav aria-label="breadcrumb" style="font-size: 14px">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard.php" style="color: #000; text-decoration: none; font-weight: 500">Profile</a></li>
              </ol>
          </nav>

          <?php
            if(isset($_GET['success_msg'])){
          ?>
                <div class="alert alert-success alert-block mx-3">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
                </div>

          <?php }elseif(isset($_GET['fail_msg'])){ ?>

              <div class="alert alert-danger alert-block mx-3">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
                </div>
          <?php } ?>

          <h5 class="text-uppercase ml-3 mt-4 mb-3">Personal Details</h5>
          <div class="bg-white"> 
            <table class="table px-3">
              <?php
                include "../inc/db_conn.php";

                $get_member = "SELECT * FROM team WHERE id = '$_SESSION[id]'";
                $run_get_member = mysqli_query($conn, $get_member);

                while($res_get_member = mysqli_fetch_array($run_get_member)){

              ?>
              <tr>
                <td width="200px"><i class="far fa-user"></i> &nbsp; First Name</td>
                <td><?php echo $res_get_member['fname'] ?></td>
              </tr><tr>
                <td><i class="fas fa-user-alt"></i> &nbsp; Full Name</td>
                <td><?php echo $res_get_member['full_name'] ?></td>
              </tr>
              <tr>
                <td><i class="fas fa-id-badge"></i> &nbsp; Employee ID</td>
                <td><?php echo $res_get_member['emp_id'] ?></td>
              </tr>
              <tr>
                <td style="font-size: 15px"><i class="fas fa-user-tie"></i> &nbsp; Date of Appointment</td>
                <td><?php echo $res_get_member['date_of_appointment'] ?></td>
              </tr>
              <tr>
                <td><i class="fas fa-briefcase"></i> &nbsp; Designation</td>
                <td><?php echo $res_get_member['designation'] ?></td>
              </tr>
              <tr>
                <td><i class="fas fa-birthday-cake"></i> &nbsp; DOB</td>
                <td><?php echo $res_get_member['dob'] ?></td>
              </tr>
              <tr>
                <td><i class="fas fa-mobile-alt"></i> &nbsp; Contact No</td>
                <td><?php echo $res_get_member['contact'] ?></td>
              </tr>
              <tr>
                <td><i class="fas fa-map-marker"></i> &nbsp; Address</td>
                <td><?php echo $res_get_member['address'] ?></td>
              </tr>
              <tr>
                <td><i class="fab fa-skype"></i> &nbsp; Skype ID</td>
                <td><?php echo $res_get_member['skype'] ?></td>
              </tr>
              <tr>
                <td><i class="fas fa-envelope"></i> &nbsp; Office E-mail</td>
                <td><?php echo $res_get_member['mail'] ?></td>
              </tr>
            </table> 
            <?php } ?>       
          </div>

        </div>  
      </div>    
    </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?> 