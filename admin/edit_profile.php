<?php
session_start();
if(!isset($_SESSION['team_mail'])){
   header('location:../home.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/admin_side_bar.php";
?>        

        <div class="col-sm-2 col-md-9 col-lg-10 col-xl-10 bg-light px-0">
          <nav aria-label="breadcrumb" style="font-size: 14px">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="dashboard.php" style="color: #000; text-decoration: none; font-weight: 500">Profile</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
              </ol>
          </nav>

          <h5 class="text-uppercase ml-3">Edit Attendance</h5>
          <div class="bg-white p-3"> 
            <form action="profile_query/edit_profile_query.php" method="POST" enctype="multipart/form-data" class="needs-validation mt-3" novalidate>
              <table class="table">
                <?php
                  include "../inc/db_conn.php";

                  $get_member = "SELECT * FROM team WHERE id = '$_SESSION[id]'";
                  $run_get_member = mysqli_query($conn, $get_member);

                  while($res_get_member = mysqli_fetch_array($run_get_member)){

                ?>
                <tr>
                  <td colspan="2" >
                    <img src="../img/team/<?php echo $res_get_member['image'] ?>" width="170px" height="200px">
                    <input class="d-block mt-3" type="file" name="img">
                  </td>
                </tr>
                <tr>
                  <td width="200px"><i class="far fa-user"></i> &nbsp; First Name</td>
                  <td><input type="text" name="fname" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['fname'] ?>" placeholder="" required></td>
                </tr>
                <tr>
                  <td width="200px"><i class="fas fa-user-alt"></i> &nbsp; Full Name</td>
                  <td><input type="text" name="full_name" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['full_name'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td><i class="fas fa-id-badge"></i> &nbsp; Employee ID</td>
                  <td><input type="text" name="emp_id" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['emp_id'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td style="font-size: 15px"><i class="fas fa-user-tie"></i> &nbsp; Date of Appointment</td>
                  <td><input type="text" name="doa" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['date_of_appointment'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td><i class="fas fa-briefcase"></i> &nbsp; Designation</td>
                  <td><input type="text" name="designation" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['designation'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td><i class="fas fa-birthday-cake"></i> &nbsp; DOB</td>
                  <td><input type="text" name="dob" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['dob'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td><i class="fas fa-mobile-alt"></i> &nbsp; Contact No</td>
                  <td><input type="text" name="contact" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['contact'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td><i class="fas fa-map-marker"></i> &nbsp; Address</td>
                  <td><input type="text" name="address" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['address'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td><i class="fab fa-skype"></i> &nbsp; Skype ID</td>
                  <td><input type="text" name="skype" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['skype'] ?>" placeholder=""></td>
                </tr>
                <tr>
                  <td><i class="fas fa-envelope"></i> &nbsp; Office E-mail</td>
                  <td><input type="email" name="mail" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" value="<?php echo $res_get_member['mail'] ?>" placeholder="" required></td>
                </tr>
                <tr>
                  <td colspan="2">
                    <input type="hidden" name="id" value="<?php echo $res_get_member['id'] ?>">
                    <button class="btn btn-success w-100 mt-3" type="submit" name="submit">Update Profile</button>
                  </td>
                </tr>
              <?php } ?>
              </table>
            </form>        
          </div>

        </div>  
      </div>    
    </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?> 