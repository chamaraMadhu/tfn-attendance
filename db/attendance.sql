-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2019 at 10:45 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `attendance`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `w_date` date NOT NULL,
  `punch_in` varchar(10) NOT NULL,
  `punch_out` varchar(10) NOT NULL,
  `lunch_in` varchar(10) NOT NULL,
  `lunch_out` varchar(10) NOT NULL,
  `break_in` varchar(10) NOT NULL,
  `break_out` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `name`, `w_date`, `punch_in`, `punch_out`, `lunch_in`, `lunch_out`, `break_in`, `break_out`) VALUES
(13, 'Chamara', '2019-02-18', '10.12 PM', '6.15 AM', '1.44 AM', '2.15 AM', '-', '-'),
(14, 'Saumya', '2019-02-18', '10.12 PM', '6.00 AM', '1.44 AM', '2.15 AM', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(2) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `emp_id` varchar(10) NOT NULL,
  `date_of_appointment` varchar(15) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `dob` varchar(20) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `address` varchar(150) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `skype` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `fname`, `full_name`, `emp_id`, `date_of_appointment`, `designation`, `image`, `dob`, `contact`, `address`, `mail`, `password`, `skype`) VALUES
(1, 'Chamara', 'Neluwe Liyanage Chamara Madhushanka Gunahilaka', '', '01 Feb 2019', 'Web Developer', 'chamara.jpg', '06 Nov 1993', '0713907528', 'No:77/A, Buthgamuwa Road, Kalapaluwawa,, Rajagiriya', 'chamara@thefuturenet.com', 'Q2hhdGh1cmFuaTFA', 'tfnchamara'),
(2, 'Saumya', ' Saumya Thejani Jayawickrama', '', '01 Feb 2019', '', 'saumya.jpg', '', '', '', 'saumya@thefuturenet.com', 'c3Nzcw==', ''),
(4, 'Lakmal', '', '', '', '', 'lakmal.jpg', '', '0712499044', '', 'lakmal@thefuturenet.com', 'bGxsbA==', ''),
(5, 'Asanka', '', '', '', '', 'default.png', '', '', '', 'asanka@thefuturenet.com', 'YWFhYQ==', ''),
(6, 'Chandima', '', '', '', '', 'default.png', '', '', '', 'chandima@thefuturenet.com', 'Y2NjYw==', ''),
(7, 'Rohan', '', '', '', '', 'default.png', '', '', '', 'rohan@thefuturenet.com', 'cnJycg==', ''),
(8, 'Nushra', '', '', '', '', 'default.png', '', '', '', 'nushra@thefuturenet.com', 'bm5ubg==', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(2) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `mail`, `password`) VALUES
(1, 'tfn@gmail.com', 'TitJfVs5NXNTbX57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
