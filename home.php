<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:index.php');
}
?>

<?php 
  include "inc/head.php";
  include "inc/navbar.php";
?>
    <!-- breadcrumb -->
    <nav aria-label="breadcrumb" style="font-size: 14px">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="home.php" style="color: #000; text-decoration: none; font-weight: 500">Home</a></li>
        </ol>
    </nav>
    <!-- breadcrumb -->

    <?php
      if(isset($_GET['success_msg'])){
    ?>
        <div class="alert alert-success alert-block container">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong><?php echo $_GET['success_msg']; ?> </strong> 
        </div>

    <?php }elseif(isset($_GET['fail_msg'])){ ?>

        <div class="alert alert-danger alert-block container">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong><?php echo $_GET['fail_msg']; ?></strong> 
        </div>
    <?php } ?>

    <!-- content -->
    <div class="container my-4">
      <div class="row">
        <div class="col-md-4 mt-3">
          <form action="home.php" method="GET" class="needs-validation" novalidate>
              <div class="form-row">
                  <div class="w-100 mb-3">
                    <label for="validationCustomUsername"><h4 class="text-muted mb-3">Check Attendance</h4></label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-calendar-alt"></i></span>
                      </div>
                      <input type="date" name="check_date" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" required>
                      <div class="invalid-feedback">
                        Please choose a date that you want to check.
                      </div>
                    </div>
                  </div>
                </div>
                
                <button class="btn btn-warning w-100 mt-2" type="submit"><i class="fas fa-search"></i> Check Attendance</button>
          </form>

          <?php

          // date_default_timezone_set("America/New_York");
          date_default_timezone_set("Asia/Colombo");
          $check_date = date("Y-m-d");

          if (!empty($_GET['check_date'])) {
                $check_date = $_GET['check_date'];
              }
          ?>

          <?php
          include "inc/db_conn.php";

              $get_attendance = "SELECT COUNT(name) AS count_att FROM attendance JOIN team WHERE attendance.name = team.fname AND w_date = '$check_date'";
              $run_get_attendance = mysqli_query($conn, $get_attendance);

              while($res_get_attendance = mysqli_fetch_array($run_get_attendance)){
          
            $count_team = "SELECT * FROM team";
            $run_count_team  = mysqli_query($conn,$count_team );
            $res_count_team  = mysqli_num_rows($run_count_team);

          ?>

           <h4 class="mt-4"><?php echo $res_get_attendance['count_att'] ?> / <?php echo $res_count_team; ?> = <?php echo round((($res_get_attendance['count_att']/$res_count_team)*100),2); } ?>%</h4><span id="indicatorContainer"></span>

        </div>
          
        <div class="col-md-8 mt-3">
          <h5><img src="img/srilanka.png" class="rounded-circle d-inline mr-2" width="30px" height="30px"> USA &nbsp; <?php echo $check_date ?></h5>
          <table class="table text-center table-hover mt-3" style="font-size: 14px">
            <tr class="bg-secondary text-light">
              <td>Emp Name</td>
              <td>Punch In</td>
              <td>Punch Out</td>
              <td>Lunch In</td>
              <td>Lunch Out</td>
              <td>Break In</td>
              <td>Break Out</td>
            </tr>

            <?php
          
              $get_attendance = "SELECT * FROM attendance JOIN team WHERE attendance.name = team.fname AND w_date = '$check_date'";
              $run_get_attendance = mysqli_query($conn, $get_attendance);

              while($res_get_attendance = mysqli_fetch_array($run_get_attendance)){

            ?>
            <tr>
              <td class="text-left text-capitalize"><img src="img/team/<?php echo $res_get_attendance['image'] ?>" class="rounded-circle" width="30px" height="30px"> &nbsp; <?php echo $res_get_attendance['name'] ?></td>
              <td><?php echo $res_get_attendance['punch_in'] ?></td>
              <td><?php echo $res_get_attendance['punch_out'] ?></td>
              <td><?php echo $res_get_attendance['lunch_in'] ?></td>
              <td><?php echo $res_get_attendance['lunch_out'] ?></td>
              <td><?php echo $res_get_attendance['break_in'] ?></td>
              <td><?php echo $res_get_attendance['break_out'] ?></td>
            </tr>
            <?php } ?>
          </table>
          
        </div>    
      </div>
    </div>
    <!-- content -->

    <!-- GPA --> 
    <script src="js/radialIndicator.js"></script>
    <script src="js/angular.radialIndicator.js"></script>

    <script>
        $('#indicatorContainer').radialIndicator({
            radius: 25,
            barColor: '#87CEEB',
            barWidth: 4,
            initValue: 100,
            roundCorner : true,
            percentage: true
        });

        var radialObj = $('#indicatorContainer').data('radialIndicator');
        //now you can use instance to call different method on the radial progress.
        //like
        radialObj.animate(85);
    </script>
    <!-- GPA --> 

<?php 
  include "inc/footer.php";
?>