<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- datatable -->
    <link href="css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- datatable -->

    <title>TFN Attendance System</title>
  </head>

  <body class="bg-dark">

    <div class="container-fluid">
      <form class="px-4 py-3 border col-sm-3 col-md-5 col-lg-4 bg-white needs-validation" novalidate method="POST" action="login_logout_query/login_query.php" style="margin:auto;margin-top:150px; border-radius: 2%">
        <p class="text-center text-muted mt-2" style="font-size: 25px"><b class="text-info">TFN</b> Attendance System</p>

        <?php
        if(isset($_GET['logout_msg'])){
        ?>
          <div class="alert alert-success alert-block" style="line-height: 15px">
              <button type="button" class="close" data-dismiss="alert" style="font-size: 15px">x</button>
              <strong><?php echo $_GET['logout_msg']; ?> </strong> 
          </div>

        <?php } ?>

        <?php
        if(isset($_GET['invalid_msg'])){
        ?>
          <div class="alert alert-danger alert-block" style="line-height: 15px">
              <button type="button" class="close" data-dismiss="alert" style="font-size: 15px">x</button>
              <strong><?php echo $_GET['invalid_msg']; ?> </strong> 
          </div>

        <?php } ?>

        <?php
        if(isset($_GET['bypass_msg'])){
        ?>
          <div class="alert alert-danger alert-block"  style="line-height: 15px">
              <button type="button" class="close" data-dismiss="alert" style="font-size: 15px">x</button>
              <strong><?php echo $_GET['bypass_msg']; ?> </strong> 
          </div>

        <?php } ?>

        <div class="form-row">
          <div class="col-md-12 mb-3">
            <label for="validationCustomUsername">Mail</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-envelope"></i></span>
              </div>
              <input type="email" name="email" class="form-control" id="validationCustomUsername" placeholder="Mail" aria-describedby="inputGroupPrepend" required>
              <div class="invalid-feedback">
                Please insert mail.
              </div>
            </div>
          </div>
        </div>
        <div class="form-row">
          <div class="col-md-12 mb-4">
            <label for="validationCustomUsername">Password</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" name="password" class="form-control" id="validationCustomUsername" placeholder="Password" aria-describedby="inputGroupPrepend" required>
              <div class="invalid-feedback">
                Please insert your password.
              </div>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-sm mr-2" style="background-color: gray; color: #fff"><i class="fas fa-sign-in-alt"></i> Sign in</button>
        <button class="btn btn-sm btn-danger" type="reset"><i class="fas fa-sync-alt"></i> Reset</button>

        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- form validation -->
    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();
    </script>  
    <!-- form validation -->

  </body>
</html>