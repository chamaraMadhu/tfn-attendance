<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:index.php');
}
?>

<?php 
  include "inc/head.php";
  include "inc/navbar.php";
?>
    
    <!-- breadcrumb -->
    <nav aria-label="breadcrumb" style="font-size: 14px">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="home.php" style="color: #000; text-decoration: none; font-weight: 500">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Profile</li>
        </ol>
    </nav>
    <!-- breadcrumb -->

    <!-- content -->
    <?php
      include "inc/profile_side_bar.php";
    ?>

        <div class="col-sm-10 bg-light">
          <div class="my-4">
            <h5 class="text-uppercase mt-4 mb-3">View attendance</h5>
            <hr>

            <div class="bg-white p-2 pt-3">
              <table id="zero_config" class="table table-hover" style="font-size: 14px">
                <thead>
                    <tr style="background-color: gray; color:#fff">
                        <td>Date</td>
                        <td>Punch In</td>
                        <td>Punch Out</td>
                        <td>Lunch In</td>
                        <td>Lunch Out</td>
                        <td>Break In</td>
                        <td>Break Out</td>
                    </tr>
                </thead>

                <tbody>
                  <?php
                    include "inc/db_conn.php";

                    $view_attendance = "SELECT * FROM attendance WHERE name = '$_GET[name]'";
                    $run_view_attendance = mysqli_query($conn, $view_attendance);

                    while($res_view_attendance = mysqli_fetch_array($run_view_attendance)){

                  ?>
                    <tr>
                      <td><?php echo $res_view_attendance['w_date'] ?></td>
                      <td><?php echo $res_view_attendance['punch_in'] ?></td>
                      <td><?php echo $res_view_attendance['punch_out'] ?></td>
                      <td><?php echo $res_view_attendance['lunch_in'] ?></td>
                      <td><?php echo $res_view_attendance['lunch_out'] ?></td>
                      <td><?php echo $res_view_attendance['break_in'] ?></td>
                      <td><?php echo $res_view_attendance['break_out'] ?></td>
                    </tr> 
                  <?php } ?>   
                </tfoot>
              </table>            
            </div>
          </div>
        </div>  
      </div>    
    </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?>